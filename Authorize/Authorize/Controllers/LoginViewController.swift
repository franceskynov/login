//
//  LoginViewController.swift
//  Authorize
//
//  Created by Franceskynov on 11/14/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    var timer: Timer?
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.errorLabel.isHidden = true
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(hiddeError), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func login(_ sender: UIButton) {
        
        UserService.login(user: User(email: emailField.text!, password: passwordField.text!), completation: { (result) -> Void in
            if result == true {
                self.performSegue(withIdentifier: "protectedVC", sender: self)
                self.errorLabel.isHidden = true
            } else {
                self.errorLabel.isHidden = false
                self.errorLabel.text = "Error the user or password dont match"
                print("invalid login")
            }
        })
        
    }
    
    
    @IBAction func signup(_ sender: UIButton) {
        
    }
    
    @objc func hiddeError() {
        self.errorLabel.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
