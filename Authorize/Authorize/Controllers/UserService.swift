//
//  UserService.swift
//  Authorize
//
//  Created by Franceskynov on 11/14/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UserService {
    
    static let header = [
        "Content-Type": "application/json; charset=utf-8"
    ]


    static func login(user: User, completation: @escaping(Bool?) -> Void) {
        
        let body: [String: Any] = [
            "email": user.email!, 
            "password": user.password!
        ]
        
        Alamofire.request(Urls.urlLogin + "users", method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else { return }
                
                do {
                    
                    let json = try JSON(data: data)
                    
                    if let message = json["message"].string {
                        print(message)
                    }
                    
                    if let code = json["code"].int {
                        print(code)
                        
                        if code == 200 {
                            completation(true)
                        } else {
                            completation(false)
                        }
                    }
                    
                } catch {
                    debugPrint(error)
                }

            } else {
                completation(false)
            }
        }
    }
    
    
    func singup() {
        
    }
}
