//
//  User.swift
//  Authorize
//
//  Created by Franceskynov on 11/14/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation

class User {
    
    var email: String?
    var password: String?
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
}
